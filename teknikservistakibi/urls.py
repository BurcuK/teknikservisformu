from django.conf.urls import url
from django.contrib import admin
from django.views.generic import TemplateView
from django.conf import settings
from django.conf.urls.static import static
from servisformu.views import sayfayiyazdir,formdurumusorgula,xmlcikart


#templateview hazır fonksiyondur ve view gibi kullanarak direk html dosyasına
#yönlendirme yapabiliriz

urlpatterns = [
    url(r'^$', TemplateView.as_view(template_name="index.html")),
    #bu fonksiyonu kullanabilmemiz için templates klasöründe
    #index.html adında bir dosya oluşturmuş olmalıyız
    url(r'^kn/$', formdurumusorgula, name='formdurumusorgula'),
    #form
    url(r'^robots\.txt', TemplateView.as_view(template_name='robots.txt', content_type='text/plain')),
    url(r'^sitemap\.xml', TemplateView.as_view(template_name='sitemap.xml', content_type='application/xml')),
    url(r'^yazdir/([\w\-]+)/$', sayfayiyazdir, name='sayfayiyazdir'),
    url(r'^xml/$', xmlcikart, name='xmlcikart'),
    url(r'^urunler\.xml$', xmlcikart, name='urunlerxml'),
    url(r'^test/$', TemplateView.as_view(template_name="test.html")),
    #url(r'^test/$', test),
    url(r'^admin/', admin.site.urls),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

#/yazdır/*** diye bir link geldiğinde servisformu uygulamamızdaki views.py deki
#sayfayiyazdir fonksiyonunu çağıracağız
#([\w\-]+) her şeyi yazabiliriz