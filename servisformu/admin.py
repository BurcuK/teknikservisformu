from django.contrib import admin
from .models import Musteriler, Teknisyen, Durumlar, Aksesuarlar, ServisForm, Urunler
#models.pydeki teknisyen toblosunu ve alanları ekliyoruz


def SecilileriGuncelle1(modeladmin, request, queryset):
    print(queryset.query)
    #querysetten neler geldiğine bakıyoruz
    for k in queryset:
        k.Yetkili=k.Yetkili+"_____"
        print(k)
        #kayıtları donguden tek tek alıyoruz
        k.save()

    return ""
    #geridon ama bos don

SecilileriGuncelle1.short_description=u'Secilileri Guncelle'


def SecilileriGuncelle2(modeladmin, request, queryset):
    print(queryset.query)
    #querysetten neler geldiğine bakıyoruz
    for k in queryset:
        k.AdSoyad=k.AdSoyad+"_____"
        print(k)
        #kayıtları donguden tek tek alıyoruz
        k.save()

    return ""
    #geridon ama bos don

SecilileriGuncelle2.short_description=u'Secilileri Guncelle'

class TeknisyenAdmin(admin.ModelAdmin):
    #admin sayfasında gösterilecek detaylar
    list_display = ['Aktif', 'AdSoyad', 'Yazdir', 'EkAlanTest']
    #sırayla gösterilecek alanlar

    list_filter = ['Aktif', 'AdSoyad']
    #filtre yapabilmemiz için hazır sorgu alanı

    search_fields = ['AdSoyad']
    #Arama yapabilmemiz için otomatik bir metin alanı oluşturur.
    #bu metin alanına girdiğimiz kelimeler AdSoyad'a girdiğimiz kayıtlarda
    #sorgulanacak

    date_hierarchy = 'KayitTarihi'
    #kayıtlara yıl ay ve gün olarak otomatik filtreleme yaptırmak için gird
    #üstünde listeleme yapar

    list_per_page = 20
    #sayfadaki kayıt adeti. otomatik sayfalama yapacak
    actions = [SecilileriGuncelle2, ]

    exclude = ('KayitTarihi',)
    #kayıt tarihi alanını gizliyoruz

    def has_add_permission(self, request):

        if not request.user.is_superuser:
            try:
                KayitSayisi=self.model.objects.count()
                #hiç kayıt olmadığında hata verecek
            except:
                KayitSayisi=0
                #gecerli uygulamadaki object sayısı, yani toplam kayıt sayısı
            KalanLimit=5
            #super olmayan k. için kayıt sayısı en fazla 5 olabilir

            if KayitSayisi>=KalanLimit:
                return False
            else:
                return True

        return True

    def get_list_display(self, request):
        g_l=super(TeknisyenAdmin, self).get_list_display(request)
        #list_display listesini g_l listesine aktarıyoruz
        try:
        #kullanıcı tekrar sayfayı yuklediginde silinen alan yeniden silinmeye
        #çalışılacak ve hata oluşacak
            if not request.user.is_superuser:
                g_l.remove('EkAlanTest')
                #list_diplay listesinde EkAlanTest alanını gizle
        except:
            #hata oldugunda pass gec
            pass

        return g_l
        #g_l listesi düzenlenmiş olarak ya da normal hali ile geri göderilsin

    def has_delete_permission(self, request, obj=None):
        return False


class MusterilerAdmin(admin.ModelAdmin):
    list_display=('Kodu', 'Unvan', 'Yetkili', 'Aktif', 'AramaYap')
    list_per_page = 80
    exclude = ('KayitTarihi',)
    search_fields = ('Yetkili', 'Unvan')
    list_display_links = ('Unvan', 'Yetkili')
    actions=[SecilileriGuncelle1,]
    actions_on_top = True

class DurumlarAdmin(admin.ModelAdmin):
    list_display=('durumu',)
    list_per_page = 80

class AksesuarlarAdmin(admin.ModelAdmin):
    list_display = ('Adi',)
    list_per_page = 3

class UrunlerInline(admin.StackedInline):
    model=Urunler
    #hangi model
    extra = 0
    #yeni eklemek istendiğinde kac tane urun ekleme yapsın.
    #ilk kayıtta hazır olarak üürn ekleme sayfası getirmeyecek biz ekle diyince 1 tane urun ekleme sayfası
    #açılacak
    max_num = 5

class ServisFormAdmin(admin.ModelAdmin):
    inlines = [UrunlerInline,]
    list_display = ('FormNo', 'Musteri', 'TeslimEden', 'TeslimAlan', 'KayitTarihi', 'Yazdir')
    list_per_page = 50
    ordering = ('-KayitTarihi',)
    date_hierarchy = 'KayitTarihi'
    search_fields = ('FormNo', 'Musteri___Yetkili', 'Musteri___Telefon')
    exclude = ('KayitTarihi',)

    def formfield_for_choice_field(self, db_field, request, **kwargs):
        if db_field.name=='TeslimAlan':
            kwargs["queryset"]=Teknisyen.objects.filter(Aktif=True)
        if db_field.name=='Musteri':
            kwargs["queryset"]=Musteriler.objects.filter(Aktif=True)
        return super(ServisFormAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)






admin.site.register(ServisForm, ServisFormAdmin)
admin.site.register(Aksesuarlar, AksesuarlarAdmin)
admin.site.register(Durumlar, DurumlarAdmin)
admin.site.register(Musteriler, MusterilerAdmin)
admin.site.register(Teknisyen, TeknisyenAdmin)
#teknisyen ve teknisyenadmin sınıflarını kayıt ettiriyoruz