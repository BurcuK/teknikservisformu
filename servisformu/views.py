from django.shortcuts import render
from .models import Urunler, ServisForm, Teknisyen
from django.http import HttpResponse

def sayfayiyazdir(request, idsi):
    formdurumu=ServisForm.objects.get(id=idsi)
    formbilgileri=Urunler.objects.filter(Servisformu__id=idsi)
    #build e ait urunlerin listesi
    #servisformu__id ile gelen id yi indexlediğimiz servisformu tablosundaki id ile eşleşştiriyoruz

    return render(request, 'yazdir.html', {'formbilgileri':formbilgileri, 'formdurumu':formdurumu})

    #bulduğumuz bilgileri yazdir.html e gönderiyoruz. template içinde

#anasayfadan gelen sorguya cevap için bir yönlendirme yapalım

def formdurumusorgula(request):
    formno=request.POST.get('TakipNo', None)
    print(formno)
    #post ile gelen TakipNo yu formno değişkenine alıyoruz.
    #takipno parametresi yoksa none döndürür
    try:
        formdurumu=ServisForm.objects.get(FormNo=formno)
        urunler=Urunler.objects.filter(Servisformu__id=formdurumu.id).all()

        return render(request, 'tablo.html', {'urunler':urunler, 'formdurumu':formdurumu})

    except:
        return HttpResponse("Form No BULUNAMADI!")
    return HttpResponse("Form No Bulunamadı!")

def xmlcikart(request):
    tumformlar=ServisForm.objects.order_by('-KayitTarihi')
    #kayıt tarihlerini sondan basa sıralayarak 2 tanesini al. son 2 kaydı
    from django.core.urlresolvers import resolve
    GelenUrl=resolve(request.path_info).url_name
    if GelenUrl=="urunlerxml":
        print(tumformlar)
        tumformlar=tumformlar[1:3]
        print(tumformlar)
    return render(request, 'formlar.xml', {'tumformlar':tumformlar}, content_type="application/xml")
    #toparladığımız son 2 kaydı formlar.xml e gönderiyoruz

def test(request):
    list1 =['Django', 'Reindhardt', '', True]
    list2 =['digerliste1', 'digerliste2', None]
    string="bu bir string"
    integer=35

    return render(request, 'test.html', {'list1': list1,
                                         'list2': list2,
                                         'string': string,
                                         'integer':integer},)
