from __future__ import unicode_literals
import random, string
from django.db import models
from django.db import models
from django.utils import timezone
from django.utils.safestring import mark_safe


class Teknisyen(models.Model):
    #teknisyen adında bir tablo oluşturduk
    Aktif=models.BooleanField(default=1)
    #teknisyen aktif mi değil mi? default olarak seçili gelecek
    AdSoyad=models.CharField(u'Adı Soyadı', max_length=250)
    #teknisyenin ad ve soyadını gireceğimiz varchar alanı
    KayitTarihi=models.DateField(u'Kayit Tarihi', default=timezone.now)
    #kayıt işlemi yapıldığında otomatik bugunu sececek ve gözükmeyecek

    def __str__(self):
    #models çağrıldığında burada seçilen alanlar döndürülür
        return self.AdSoyad
    #admin.py'de list_displayde eklediğimiz alanlar ve sıralamasıyla oluşmakta

    class Meta:
        #admin sayfasında bu uygulama nasıl isimlendirilip çağrılaak?
        verbose_name_plural=u'Teknisyenler'
        verbose_name=u'teknisyen'

    def Yazdir(self):
        return mark_safe('<a href="/yazdir/%s" target="_blank">Yazdır</a>' % self.id)
    #fonksiyon çağrıldığında döndürülecek metin

    Yazdir.short_description=u'Yazdir'
    #fonksiyonun kısa açıklaması

    Yazdir.allows_tags=True
    #fonksiyonumuz html etiket içeriyor

    def EkAlanTest(self):
        return self.AdSoyad.replace(' ','_______')

    EkAlanTest.short_description=u'Burası alanın baslıgı'

class Musteriler(models.Model):
    Aktif=models.BooleanField(default=1)
    Kodu=models.CharField(u'Musteri Kodu', default=''.join(random.choice(string.digits)for x in range(8)), max_length=8)
    Unvan=models.CharField(u'Ticari Unvan', max_length=250)
    Yetkili=models.CharField(u'Yetkili Adı Soyadı', max_length=250)
    Telefon=models.CharField(u'Telefon', max_length=13, blank=True)
    KayitTarihi=models.DateField(u"Kayit Tarihi", default=timezone.now)

    def __str__(self):
        return u'%s %s' % (self.Kodu, self.Yetkili)

    class Meta:
        verbose_name_plural = u'Musteriler'
        verbose_name=u'Musteri'

    def AramaYap(self):
        if self.Telefon:
            return mark_safe('<a href="tel:%s" target="_blank">Numarayı Ara</a>' % self.Telefon)
        else:
            return 'Telefon No kayıt edilmedi'

    AramaYap.short_description = u'Ara'
    AramaYap.allows_tags = True

class Durumlar(models.Model):
    durumu=models.CharField(u'Durum', max_length=30, help_text='Urununuzun sorununu bize bildirin')

    def __str__(self):
        return self.durumu

    class Meta:
        verbose_name_plural=u'Durumlar'
        verbose_name=u'Durum'

class Aksesuarlar(models.Model):
    Adi=models.CharField(u'Adi', max_length=30, help_text='Urunle beraber hangi aksesuarlar geldi?')

    def __str__(self):
        return self.Adi

    class Meta:
        verbose_name_plural=u'Aksesuarlar'
        verbose_name=u'Aksesuar'


class ServisForm(models.Model):
    Musteri=models.ForeignKey(Musteriler, related_name="musteri")
    TeslimAlan=models.ForeignKey(Teknisyen, default=int(Teknisyen.objects.get(id=1).id))
    TeslimEden=models.CharField(u'Teslim Eden', max_length=130)
    FormNo=models.CharField(u'Form No', default=''.join(random.choice(string.digits) for x in range(8)), max_length = 8 )
    KayitTarihi=models.DateTimeField(u'Kayit Tarihi', default=timezone.now)

    def __str__(self):
        return self.FormNo

    class Meta:
        verbose_name_plural=u'Formlar'
        verbose_name=u'Servis Formu'

    def Yazdir(self):
        return mark_safe('<a href="/yazdir/%s" target="_blank">Yazdır</a>' % self.id)
    #fonksiyon çağrıldığında döndürülecek metin

    Yazdir.short_description=u'Yazdir'
    #fonksiyonun kısa açıklaması

    Yazdir.allows_tags=True
    #fonksiyonumuz html etiket içeriyor

class Urunler(models.Model):
    Servisformu=models.ForeignKey(ServisForm)
    Cins=models.CharField(u'Cinsi', max_length=30)
    Marka=models.CharField(u'Marka', max_length=50)
    Model=models.CharField(u'Model', max_length=50)
    SeriNo=models.CharField(u'Seri No', max_length=250)
    GarantiBitis=models.DateField(u'Garanti Bitis', default=timezone.now)
    Sikayet=models.TextField(u'Sikayet')
    Aksesuar=models.ManyToManyField(Aksesuarlar, blank=True)
    Durum=models.ForeignKey(Durumlar)
    Not=models.TextField(u'Yapilan İslemler', blank=True)
    TeslimatTarihi=models.DateField(u"Teslimat Tarihi", default=timezone.now, blank=True)

    def __str__(self):
        return "%s %s %s" % (self.Cins, self.Marka, self.Model)

    class Meta:
        verbose_name_plural = u'Urunler'
        verbose_name=u'Urun'








