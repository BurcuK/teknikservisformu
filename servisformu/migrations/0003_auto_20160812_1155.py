# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2016-08-12 11:55
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('servisformu', '0002_auto_20160812_1150'),
    ]

    operations = [
        migrations.AlterField(
            model_name='musteriler',
            name='Kodu',
            field=models.CharField(default='87931818', max_length=8, verbose_name='Musteri Kodu'),
        ),
    ]
