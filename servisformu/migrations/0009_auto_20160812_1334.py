# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2016-08-12 13:34
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('servisformu', '0008_auto_20160812_1329'),
    ]

    operations = [
        migrations.AlterField(
            model_name='musteriler',
            name='Kodu',
            field=models.CharField(default='21243732', max_length=8, verbose_name='Musteri Kodu'),
        ),
        migrations.AlterField(
            model_name='servisform',
            name='FormNo',
            field=models.CharField(default='81009503', max_length=8, verbose_name='Form No'),
        ),
        migrations.AlterField(
            model_name='servisform',
            name='Musteri',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='musteri', to='servisformu.Musteriler'),
        ),
    ]
