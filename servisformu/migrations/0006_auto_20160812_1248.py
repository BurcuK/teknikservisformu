# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2016-08-12 12:48
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('servisformu', '0005_auto_20160812_1243'),
    ]

    operations = [
        migrations.CreateModel(
            name='Aksesuarlar',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Adi', models.CharField(help_text='Urunle beraber hangi aksesuarlar geldi?', max_length=30, verbose_name='Adi')),
            ],
            options={
                'verbose_name_plural': 'Aksesuarlar',
                'verbose_name': 'Aksesuar',
            },
        ),
        migrations.AlterField(
            model_name='musteriler',
            name='Kodu',
            field=models.CharField(default='35165346', max_length=8, verbose_name='Musteri Kodu'),
        ),
    ]
